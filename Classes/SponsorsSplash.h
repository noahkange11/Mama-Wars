#ifndef __SPONSORS_H__
#define __SPONSORS_H__

#include "cocos2d.h"
#include "Definitions.h"
//#include "NandoziSplash.h"

using namespace cocos2d;

class SponsorsSplash : public Layer
{

 public:

    static Scene* createScene();

  virtual bool init();

  CREATE_FUNC(SponsorsSplash);

private:

 void GoToNandoziSplash(float dt);


};
#endif
