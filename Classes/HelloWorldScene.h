#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"


#include "GameLayer.h"

using namespace cocos2d;
using namespace ui;

enum {ACTION_ENG, ACTION_FR, ACTION_AR, ACTION_SW};

class HelloWorld : public cocos2d::Scene
{
private:
  Director* _director;
  float _screenWidth;
  float _screenHeight;
    Scene* scene;


  Size screenSize;
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    void processButtonPressed(Ref*);

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
