#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "CutScene1.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    //THE ISH
    _director = Director::getInstance();

    screenSize = _director->getVisibleSize();
    Vec2 origin = _director->getVisibleOrigin();
    _screenHeight = screenSize.height;
    _screenWidth = screenSize.width;



    auto rad = LayerGradient::create(Color4B(0, 68, 170, 183), Color4B(215, 238, 244, 255));
    this->addChild(rad, 0);

    Button* enButton = Button::create("rateUs.png");
    enButton->setActionTag(ACTION_ENG);
    enButton->setTitleText("ENGLISH");
    enButton->setTitleFontSize(45);
    enButton->setTitleColor(Color3B::BLACK);
    enButton->setPosition(Vec2(_screenWidth*0.5, _screenHeight*0.8));
    enButton->addTouchEventListener(CC_CALLBACK_1(HelloWorld::processButtonPressed,this));
    //enButton->setPressedActionEnabled(true);
    this->addChild(enButton, 1);

    Button* frButton = Button::create("rateUs.png");
    frButton->setActionTag(ACTION_FR);
    frButton->setTitleText("FRANÇAIS");
    frButton->setTitleFontSize(45);
    frButton->setTitleColor(Color3B::BLACK);
    frButton->setPosition(Vec2(_screenWidth*0.5, _screenHeight*0.6));
    frButton->addTouchEventListener(CC_CALLBACK_1(HelloWorld::processButtonPressed,this));
    this->addChild(frButton, 1);

    Button* arButton = Button::create("rateUs.png");
    arButton->setActionTag(ACTION_AR);
    arButton->setTitleText("العربية");
    arButton->setTitleFontSize(45);
    arButton->setTitleColor(Color3B::BLACK);
    arButton->setPosition(Vec2(_screenWidth*0.5, _screenHeight*0.4));
    arButton->addTouchEventListener(CC_CALLBACK_1(HelloWorld::processButtonPressed,this));
    this->addChild(arButton, 1);


    Button* swButton = Button::create("rateUs.png");
    swButton->setActionTag(ACTION_SW);
    swButton->setTitleText("KISWAHILI");
    swButton->setTitleFontSize(45);
    swButton->setTitleColor(Color3B::BLACK);
    swButton->setPosition(Vec2(_screenWidth*0.5, _screenHeight*0.2));
    swButton->addTouchEventListener(CC_CALLBACK_1(HelloWorld::processButtonPressed,this));

    this->addChild(swButton, 1);


    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + _screenWidth - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2;
        closeItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    auto label = Label::createWithTTF("CHOOSE LANGUAGE", "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + _screenWidth/2,
                                origin.y + _screenHeight - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }






    return true;
}

void HelloWorld::processButtonPressed(Ref* psender) {



    if (((Button*)psender)->getActionTag() == ACTION_AR){



        auto scene = CutScene1::createScene("ar");
        _director->replaceScene(TransitionFade::create(0.5, scene));

    }else if(((Button*)psender)->getActionTag() == ACTION_FR){


        auto scene = CutScene1::createScene("fr");
        _director->replaceScene(TransitionFade::create(0.5, scene));

    }else if(((Button*)psender)->getActionTag() == ACTION_SW){

        auto scene = CutScene1::createScene("sw");
        _director->replaceScene(TransitionFade::create(0.5, scene));

    } else{
 //_langManager->setLanguage("en")
        auto scene = CutScene1::createScene("en");
        _director->replaceScene(TransitionFade::create(0.5, scene));
    }




    /*
       switch(((Button*)psender)->getActionTag()){
           case ACTION_AR:
               _langManager->setLanguage("ar");
                scene = HelloWorld2::createScene();
               _director->replaceScene(TransitionFade::create(0.5, scene));
               break;
           case ACTION_FR:
               _langManager->setLanguage("fr");
               scene = HelloWorld2::createScene();
               _director->replaceScene(TransitionFade::create(0.5, scene));
               break;

           case ACTION_SW:
               _langManager->setLanguage("sw");
               scene = HelloWorld2::createScene();
               _director->replaceScene(TransitionFade::create(0.5, scene));
               break;
           case ACTION_ENG:
               _langManager->setLanguage("en");
               scene = HelloWorld2::createScene();
               _director->replaceScene(TransitionFade::create(0.5, scene));
               break;
           default:
               _langManager->setLanguage("en");
               scene = HelloWorld2::createScene();
               _director->replaceScene(TransitionFade::create(0.5, scene));

       }
        */


}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);


}
