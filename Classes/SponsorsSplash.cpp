#include "SponsorsSplash.h"
#include "GameLayer.h"
#include "HelloWorldScene.h"

USING_NS_CC;

Scene* SponsorsSplash::createScene()
{

	auto scene = Scene::create();
	auto layer = SponsorsSplash::create();

	scene->addChild(layer);

return scene;
}

bool SponsorsSplash::init()
{

	if(!Layer::init())
	{
	return false;
	}

	glClearColor(1.0,1.0,1.0,1.0);
	Size _visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 _origin = Director::getInstance()->getVisibleOrigin();

  	this->scheduleOnce(schedule_selector(SponsorsSplash::GoToNandoziSplash), DISPLAY_TIME_SPLASH_SCENE);

		auto whiteBG2 = Sprite::create("bgWhite.png");
    whiteBG2->setPosition(Vec2(_visibleSize.width/2 + _origin.x, _visibleSize.height/2+_origin.y));
    this->addChild(whiteBG2,1);

 	auto bgSprite = Sprite::create("sponsors.png");
	bgSprite->setPosition(Point(_visibleSize.width/2 +_origin.x, _visibleSize.height/2+_origin.y));
	this->addChild(bgSprite,2);

  return true;
}

void SponsorsSplash::GoToNandoziSplash(float dt)
{

	auto sponscene = HelloWorld::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, sponscene));
}
