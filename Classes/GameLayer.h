//
// Created by MAC on 28/01/2018.
//

#ifndef PROJ_ANDROID_STUDIO_GAMELAYER_H
#define PROJ_ANDROID_STUDIO_GAMELAYER_H

#include "cocos2d.h"
#include "ParallaxNodeExtras.h"
#include "VisibleRect.h"
#include "LanguageManager.h"
USING_NS_CC;

typedef enum{
    KENDREASONWIN, KENDREASONLOSE
}EndReason;

class GameLayer : public Layer{
public:
    // returning the class instance pointer
    static Scene* createScene(/*string local*/);


    virtual bool init();
   // string getLocal();

    void menuCloseCallback(Ref* pSender);

    CREATE_FUNC(GameLayer);

    virtual void onAcceleration(Acceleration* acc, Event* event);
    float randomValueBetween(float low, float high);
    void setInvisible(Node* node);
    float getTimeTick();
    void onTouchesBegan(const std::vector<Touch*>& touches, Event *event);

private:
    Director* _director;
    float _screenWidth;
    float _screenHeight;
    SpriteBatchNode* _batchNode;
    Sprite* _ship;
    Sprite* _baby;
    ParallaxNodeExtras* _backgroundNode;
    Sprite* _spaceDust1, *_spaceDust2, *_planetSunrise, *_galaxy, *_spatialAnomaly1, *_spatialAnomaly2;;
    float _shipPointsPerSecY;
    Vector<Sprite*> *_asteroids;
    int _nextAsteroid = 0;
    float _nextAsteroidSpawn = 0;
    Vector<Sprite*> *_shipLasers;
    int _nextShipLaser = 0;
    int _lives = 0;
    double _gameOverTime;
    bool _gameOver = false;

    void update(float dt);
    void endScene(EndReason endReason);
    void restartTapped(Ref* pSender);
};

#endif //PROJ_ANDROID_STUDIO_GAMELAYER_H
