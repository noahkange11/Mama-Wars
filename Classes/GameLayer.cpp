//
// Created by MAC on 28/01/2018.
//

#include "GameLayer.h"
#include "SimpleAudioEngine.h"

using namespace CocosDenshion;

USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define SPACE_GAME "BugGame.caf"
#define EXPLOSION_LARGE "explosion_large.caf"
#define LASER_SHIP "laser_ship.caf"

#else

#define SPACE_GAME "hura.mp3"
#define BABY_CRY "babyCry.mp3"
#define MOSQUITOES "Mosquito.mp3"
#define EXPLOSION_LARGE "pop.mp3"
#define LASER_SHIP "laser.mp3"
#endif

//string localV;
Scene* GameLayer::createScene(/*string local*/) {

    //localV = local;
    auto scene = Scene::create();

    auto layer = GameLayer::create();

    scene->addChild(layer);

    return scene;
}

bool GameLayer::init() {

    if(!Layer::init()){
        return  false;
    }
    _director = Director::getInstance();
    Size visibleSize = _director->getVisibleSize();
    _screenHeight = visibleSize.height;
    _screenWidth = visibleSize.width;
    Point origin = _director->getVisibleOrigin();

    // add a "close" icon to exit the progress. it's an autorelease object


    auto rad =  LayerGradient::create(Color4B(0, 69, 160, 183), Color4B(215, 238, 244, 255));
    this->addChild(rad, 0);

    auto closeItem = MenuItemImage::create("CloseNormal.png", "CloseSelected.png", CC_CALLBACK_1(GameLayer::menuCloseCallback, this));
    closeItem->setPosition(Vec2(origin.x + _screenWidth - closeItem->getContentSize().width/2, origin.y + closeItem->getContentSize().height/2));

    //create menu
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu, 1);


    // Galaxy
    _batchNode = SpriteBatchNode::create("Sprites.pvr.ccz");
    this->addChild(_batchNode);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("Sprites.plist");

    _ship = Sprite::create("kale.png");
    _ship->setPosition(Vec2(_screenWidth*0.1, _screenHeight*0.5));
    this->addChild(_ship, 2);

    _baby = Sprite::create("baby.png");
    _baby->setPosition(Vec2(_screenWidth*0.1, _screenHeight*0.5));
    _baby->setScale(0.35f);
    this->addChild(_baby, 1);

    // 1 create the parallaxNode
    _backgroundNode = ParallaxNodeExtras::create();
    this->addChild(_backgroundNode, -1);


    // 2) Create the sprites will be added to the ParallaxNode
    _spaceDust1 = Sprite::create("bg_front_spacedust.png");
    _spaceDust2 = Sprite::create("bg_front_spacedust.png");
    _planetSunrise = Sprite::create("bg_planetsunrise.png");
    _galaxy = Sprite::create("bg_galaxy.png");
    _spatialAnomaly1 = Sprite::create("bg_spacialanomaly.png");
    _spatialAnomaly2 = Sprite::create("bg_spacialanomaly2.png");

    // 3) Determine relative movement speeds for space dust and background
    auto dustSpeed = Point(0.1F, 0.1F);
    auto bgSpeed = Point(0.05F, 0.05F);

    // 4) Add children to ParallaxNode
    _backgroundNode->addChild(_spaceDust1, 0, dustSpeed, Point(0, _screenHeight / 2));
    _backgroundNode->addChild(_spaceDust2, 0, dustSpeed, Point(_spaceDust1->getContentSize().width, _screenHeight / 2));
    _backgroundNode->addChild(_galaxy, -1, bgSpeed, Point(0, _screenHeight * 0.7));
    _backgroundNode->addChild(_planetSunrise, -1, bgSpeed, Point(600, _screenHeight * 0));
    _backgroundNode->addChild(_spatialAnomaly1, -1, bgSpeed, Point(900, _screenHeight * 0.3));
    _backgroundNode->addChild(_spatialAnomaly2, -1, bgSpeed, Point(1500, _screenHeight * 0.9));

    GameLayer::addChild(ParticleSystemQuad::create("Stars1.plist"));
    GameLayer::addChild(ParticleSystemQuad::create("Stars2.plist"));
    GameLayer::addChild(ParticleSystemQuad::create("Stars3.plist"));

#define KNUMASTEROIDS 15

_asteroids = new Vector<Sprite*>(KNUMASTEROIDS);
    for (int i = 0; i < KNUMASTEROIDS; ++i) {
        auto *asteroid = Sprite::create("bad.png");
        asteroid->setVisible(false);
        this->addChild(asteroid);

        _asteroids->pushBack(asteroid);

    }


#define KNUMLASERS 5
    _shipLasers = new Vector<Sprite*>(KNUMLASERS);
    for (int i = 0; i < KNUMLASERS; ++i) {
        auto shipLaser = Sprite::createWithSpriteFrameName("laserbeam_blue.png");
        shipLaser->setVisible(false);
        _batchNode->addChild(shipLaser);
        _shipLasers->pushBack(shipLaser);
    }


    Device::setAccelerometerEnabled(true);
    auto accelerationListener = EventListenerAcceleration::create(CC_CALLBACK_2(GameLayer::onAcceleration, this));
    _eventDispatcher->addEventListenerWithSceneGraphPriority(accelerationListener, this);

    auto touchListener = EventListenerTouchAllAtOnce::create();
    touchListener->onTouchesBegan = CC_CALLBACK_2(GameLayer::onTouchesBegan, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    _lives = 3;
    double curTime = getTimeTick();
    _gameOverTime = curTime + 30000;

    this->scheduleUpdate();

    SimpleAudioEngine::getInstance()->playBackgroundMusic(SPACE_GAME, true);
    SimpleAudioEngine::getInstance()->playBackgroundMusic(MOSQUITOES, true);
    SimpleAudioEngine::getInstance()->preloadEffect(EXPLOSION_LARGE);
    SimpleAudioEngine::getInstance()->preloadEffect(LASER_SHIP);

    return true;

}

void GameLayer::update(float dt) {

    auto backgroundScrollVert = Point(-1000, 0);
    _backgroundNode->setPosition(_backgroundNode->getPosition() + (backgroundScrollVert * dt));

    //Parallax
    auto spaceDusts = new Vector<Sprite*>(2);
    spaceDusts->pushBack(_spaceDust1);
    spaceDusts->pushBack(_spaceDust2);
    for (auto spaceDust : *spaceDusts) {
        float xPosition = _backgroundNode->convertToWorldSpace(spaceDust->getPosition()).x;
        float size = spaceDust->getContentSize().width;
        if (xPosition < -size / 2) {
            _backgroundNode->incrementOffset(Point(spaceDust->getContentSize().width * 2, 0), spaceDust);
        }
    }

    auto backGrounds = new Vector<Sprite*>(4);
    backGrounds->pushBack(_galaxy);
    backGrounds->pushBack(_planetSunrise);
    backGrounds->pushBack(_spatialAnomaly1);
    backGrounds->pushBack(_spatialAnomaly2);
    for (auto background : *backGrounds) {
        float xPosition = _backgroundNode->convertToWorldSpace(background->getPosition()).x;
        float size = background->getContentSize().width;
        if (xPosition < -size) {
            _backgroundNode->incrementOffset(Point(2000, 0), background);
        }
    }

    //Acceleration
    Size winSize = _director->getWinSize();
    float maxY = winSize.height - _ship->getContentSize().height / 2;
    float minY = _ship->getContentSize().height / 2;
    float diff = (_shipPointsPerSecY * dt);
    float newY = _ship->getPosition().y + diff;
    newY = MIN(MAX(newY, minY), maxY);
    _ship->setPosition(_ship->getPosition().x, newY);

    float curTimeMillis = getTimeTick();
    if (curTimeMillis > _nextAsteroidSpawn) {

        float randMillisecs = randomValueBetween(0.20F, 1.0F) * 1000;
        _nextAsteroidSpawn = randMillisecs + curTimeMillis;

        float randY = randomValueBetween(0.0F, winSize.height);
        float randDuration = randomValueBetween(2.0F, 10.0F);

        Sprite *asteroid = _asteroids->at(_nextAsteroid);
        _nextAsteroid++;

        if (_nextAsteroid >= _asteroids->size())
            _nextAsteroid = 0;

        asteroid->stopAllActions();
        asteroid->setPosition(winSize.width + asteroid->getContentSize().width / 2, randY);
        asteroid->setVisible(true);
        asteroid->runAction(
                Sequence::create(
                        MoveBy::create(randDuration, Point(-winSize.width - asteroid->getContentSize().width, 0)),
                        CallFuncN::create(CC_CALLBACK_1(GameLayer::setInvisible, this)),
                        NULL /* DO NOT FORGET TO TERMINATE WITH NULL (unexpected in C++)*/)
        );
    }

    // Asteroids
    for (auto asteroid : *_asteroids){
        if (!(asteroid->isVisible()))
            continue;
        for (auto shipLaser : *_shipLasers){
            if (!(shipLaser->isVisible()))
                continue;
            if (shipLaser->getBoundingBox().intersectsRect(asteroid->getBoundingBox())){
                SimpleAudioEngine::getInstance()->playEffect(EXPLOSION_LARGE);
                shipLaser->setVisible(false);
                asteroid->setVisible(false);
            }
        }
        if (_ship->getBoundingBox().intersectsRect(asteroid->getBoundingBox())){
            asteroid->setVisible(false);
            _ship->runAction(Blink::create(1.0F, 9));
            cocos2d::Device::vibrate(1.2);
            _lives--;
        }
    }

    if (_lives <= 0) {
        _ship->stopAllActions();
        _ship->setVisible(false);
        this->endScene(KENDREASONLOSE);
    }
    else if (curTimeMillis >= _gameOverTime) {
        this->endScene(KENDREASONWIN);
    }

}
/*
string GameLayer::getLocal() {
    return localV;
}
*/

void GameLayer::onAcceleration(Acceleration* acc, Event* event) {
#define KFILTERINGFACTOR 0.1
#define KRESTACCELX -0.6
#define KSHIPMAXPOINTSPERSEC (winSize.height*0.5)
#define KMAXDIFFX 0.2

#define FIX_POS(_pos, _min, _max) \
    if (_pos < _min)        \
    _pos = _min;        \
else if (_pos > _max)   \
    _pos = _max;        \
	/*
	double rollingX;

	// Cocos2DX inverts X and Y accelerometer depending on device orientation
	// in landscape mode right x=-y and y=x !!! (Strange and confusing choice)
	acc->x = acc->y;
	rollingX = (acc->x * KFILTERINGFACTOR) + (rollingX * (1.0 - KFILTERINGFACTOR));
	float accelX = acc->x - rollingX;
	Size winSize = Director::getInstance()->getWinSize();
	float accelDiff = accelX - KRESTACCELX;
	float accelFraction = accelDiff / KMAXDIFFX;
	_shipPointsPerSecY = KSHIPMAXPOINTSPERSEC * accelFraction;
	*/



    auto pDir = Director::getInstance();

    /*FIXME: Testing on the Nexus S sometimes _ball is NULL */
    if ( _ship == NULL ) {
        return;
    }

    auto shipSize  = _ship->getContentSize();

    auto ptNow  = _ship->getPosition();
    auto ptTemp = pDir->convertToUI(ptNow);

    ptTemp.x += acc->x * 9.81f;
    ptTemp.y -= acc->y * 9.81f;

    auto ptNext = pDir->convertToGL(ptTemp);
    FIX_POS(ptNext.x, (VisibleRect::left().x+shipSize.width / 2.0), (VisibleRect::right().x - shipSize.width / 2.0));
    FIX_POS(ptNext.y, (VisibleRect::bottom().y+shipSize.height / 2.0), (VisibleRect::top().y - shipSize.height / 2.0));
    _ship->setPosition(ptNext);


}

float GameLayer::randomValueBetween(float low, float high) {
    // from http://stackoverflow.com/questions/686353/c-random-float-number-generation
    return low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
}

float GameLayer::getTimeTick() {
    timeval time;
    gettimeofday(&time, NULL);
    unsigned long millisecs = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    return (float)millisecs;
}

void GameLayer::setInvisible(Node * node) {
    node->setVisible(false);
}

void GameLayer::onTouchesBegan(const std::vector<Touch*>& touches, Event  *event){
    SimpleAudioEngine::getInstance()->playEffect(LASER_SHIP);
    auto winSize = Director::getInstance()->getWinSize();
    auto shipLaser = _shipLasers->at(_nextShipLaser++);
    if (_nextShipLaser >= _shipLasers->size())
        _nextShipLaser = 0;
    shipLaser->setPosition(_ship->getPosition() + Point(shipLaser->getContentSize().width / 2, 0));
    shipLaser->setVisible(true);
    shipLaser->stopAllActions();
    shipLaser->runAction(
            Sequence::create(
                    MoveBy::create(0.5, Point(winSize.width, 0)),
                    CallFuncN::create(CC_CALLBACK_1(GameLayer::setInvisible, this)),
                    NULL));
}

void GameLayer::restartTapped(Ref* pSender) {
    Director::getInstance()->replaceScene
            (TransitionZoomFlipX::create(0.5, this->createScene()));
    // reschedule
    this->scheduleUpdate();
}

void GameLayer::endScene(EndReason endReason) {
    if (_gameOver)
        return;
    _gameOver = true;

    auto winSize = Director::getInstance()->getWinSize();
    string message = LanguageManager::getInstance()->getStringForKey("WIN");
    if (endReason == KENDREASONLOSE)
        message = LanguageManager::getInstance()->getStringForKey("LOST");
    auto label = Label::createWithBMFont("Arial.fnt", message);
    label->setScale(0.1F);
    label->setPosition(winSize.width / 2, winSize.height*0.6F);
    this->addChild(label);

    message = LanguageManager::getInstance()->getStringForKey("RESTART");

    auto restartLabel = Label::createWithBMFont("Arial.fnt", message);
    auto restartItem = MenuItemLabel::create(restartLabel, CC_CALLBACK_1(GameLayer::restartTapped, this));
    restartItem->setScale(0.1F);
    restartItem->setPosition(winSize.width / 2, winSize.height*0.4);

    auto *menu = Menu::create(restartItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu);

    // clear label and menu
    restartItem->runAction(ScaleTo::create(0.5F, 1.0F));
    label->runAction(ScaleTo::create(0.5F, 1.0F));
    SimpleAudioEngine::getInstance()->playBackgroundMusic(BABY_CRY);

    // Terminate update callback
    this->unscheduleUpdate();
}


void GameLayer::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
    MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}






