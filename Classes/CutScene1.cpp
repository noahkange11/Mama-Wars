#include "CutScene1.h"
#include "LanguageManager.h"

USING_NS_CC;

string localV;
Scene* CutScene1::createScene(string local){

    localV = local;
  auto scene = Scene::create();
  auto layer = CutScene1::create();

  scene->addChild(layer);

  return scene;
}// end createScene()

bool CutScene1::init(){

  if (!Layer::init()) {
    return false;
  }

  _director = Director::getInstance();
  _screenSize = _director->getVisibleSize();
  Vec2 _origin = _director->getVisibleOrigin();

  //screen Size specifics
  _screenWidth = _screenSize.width;
  _screenHeight = _screenSize.height;

  //sound fo rhte background
  SimpleAudioEngine::getInstance()->playBackgroundMusic("hura.mp3", true);

    auto rad =  LayerGradient::create(Color4B(0, 69, 160, 183), Color4B(215, 238, 244, 255));
    this->addChild(rad, 0);

 auto teacher = Sprite::create("hala.png");
 teacher->setPosition(Vec2(_screenWidth/2+_origin.x, _screenHeight/2+_origin.y));
 teacher->setScale(0.89f);
 this->addChild(teacher, 1);



    auto cliBody3 = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("ON_AIR").c_str(), "fonts/Crayon.ttf", 44);
    cliBody3->setAnchorPoint(Vec2(0.5,0.5));
    cliBody3->setPosition(Vec2(_screenWidth*0.66, _screenHeight*0.85f));

   cliBody3->setColor(Color3B::ORANGE);
    this->addChild(cliBody3, 4);

    auto onair = Sprite::create("on_air.png");
    onair->setAnchorPoint(Vec2(0.5,0.5));
    onair->setPosition(Vec2(_screenWidth*0.66, _screenHeight*0.85f));

    this->addChild(onair, 3);

    auto msg = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("INTRO").c_str(), "fonts/Crayon.ttf", 44);
    msg->setPosition(Vec2(_screenWidth*0.7, _screenHeight*0.2f));
    msg->setColor(Color3B::BLACK);
    this->addChild(msg, 4);

 auto _cutSkipItem = MenuItemImage::create("buttons/skipButton.png", "buttons/skipButtonPressed.png", CC_CALLBACK_1(CutScene1::menuBackCallBack, this));
 _cutSkipItem->setAnchorPoint(Vec2(0,0));
 _cutSkipItem->setScale(0.5f);
 _cutSkipItem->setPosition(Vec2(_screenWidth*0.03f, _screenHeight*0.13f));

auto  menu = Menu::create(_cutSkipItem, NULL);
 menu->setPosition(Vec2::ZERO);

 this->addChild(menu, 3);


 auto skipLabel = Label::createWithTTF(LanguageManager::getInstance()->getStringForKey("SKIP").c_str(), "fonts/Crayon.ttf", 44);
 skipLabel->setColor(Color3B::BLACK);
 skipLabel->setPosition(Vec2(_screenWidth*0.07f, _screenHeight*0.10f));
 this->addChild(skipLabel,4);



 scheduleOnce(schedule_selector(CutScene1::skipCutScene1), 14.0);


}//end init()

string CutScene1::getLocal() {
    return localV;
}


void CutScene1::skipCutScene1(float dt){
  auto menuScene = GameLayer::createScene();
_director->replaceScene(TransitionFade::create(TRANSITION_TIME, menuScene ));
}// end skipCutScene1()


void CutScene1::menuBackCallBack(Ref* pSender){

	auto menuScene = GameLayer::createScene();
	_director->replaceScene(TransitionFade::create(TRANSITION_TIME, menuScene));
}//menuBackCallBack()
