//
// Created by MAC on 28/01/2018.
//

#ifndef PROJ_ANDROID_STUDIO_PARALLAXNODEEXTRAS_H
#define PROJ_ANDROID_STUDIO_PARALLAXNODEEXTRAS_H

#include "cocos2d.h"

USING_NS_CC;

class ParallaxNodeExtras : public ParallaxNode{
public:
    //constructor
    ParallaxNodeExtras();

    //just to avoid ugly later cast and also for safety
    static ParallaxNodeExtras* create();

    //Facility method (it's expected to have it soon in COCOS2DX)
    void incrementOffset(Point offset, Node* node);
};
#endif //PROJ_ANDROID_STUDIO_PARALLAXNODEEXTRAS_H
