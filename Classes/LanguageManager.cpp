#include "LanguageManager.h"

#include "cocos2d.h"
#include "CutScene1.h"

using namespace cocos2d;

LanguageManager* LanguageManager::_instance = 0;


LanguageManager::LanguageManager()
{

    //both statements are tests
    CutScene1* gLayer;

   this->setLanguage(gLayer->getLocal());

    langType = langName;


    if(langType == "en")
        fileName = "en.json"; // english language
    else
    if(langType  == "sw")
        fileName = "sw.json"; // Swahili language
    else
    if(langType  == "fr")
        fileName = "fr.json"; // French language
    else
    if(langType == "ar")
        fileName = "ar.json"; // arabic
        else
            fileName = "ar.json"; // default




    ssize_t size;
    const char* buf = (const char*)FileUtils::getInstance()->getFileData(fileName.c_str(), "r", &size);
    string content(buf);
    string clearContent = content.substr(0, content.rfind('}') + 1);

    _document.Parse<0>(clearContent.c_str());
    if(_document.HasParseError())
    {
        CCLOG("Language file parsing errors");
        return;
    }
}

LanguageManager::~LanguageManager(){
    langName = "en";
}

LanguageManager* LanguageManager::getInstance()
{
    if(!_instance)
        _instance = new LanguageManager();
    return _instance;
}

string LanguageManager::getStringForKey(string key) const
{
    return _document[key.c_str()].GetString();
}


 void LanguageManager::setLanguage(string name){
   langName = name;


}
/*
string LanguageManager::getLanguage(){

    return langName;

}
*/
