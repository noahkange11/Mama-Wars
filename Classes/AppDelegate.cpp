#include "AppDelegate.h"
#include "SimpleAudioEngine.h"

#include "SponsorsSplash.h"
#include "GameLayer.h"
#include "HelloWorldScene.h"
#include "CutScene1.h"

using namespace CocosDenshion;

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate()
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("MAMA");
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(false);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);


    auto screenSize = glview->getFrameSize();
    auto designSize = Size(2048, 1536);

    glview->setDesignResolutionSize(designSize.width, designSize.height, ResolutionPolicy::EXACT_FIT);
    CCLOG("SCREEN: w:%f h:%f", screenSize.width, screenSize.height);
    std::vector<std::string> searchPaths;
    if (screenSize.height > 768) {
        searchPaths.push_back("ipadhd");
        director->setContentScaleFactor(1536/designSize.height);
    } else if (screenSize.height > 320) {
        searchPaths.push_back("ipad");
        director->setContentScaleFactor(768/designSize.height);
    } else {
        searchPaths.push_back("iphone");
        director->setContentScaleFactor(380/designSize.height);

    }
    searchPaths.push_back("language");

    auto fileUtils = FileUtils::getInstance();
    fileUtils->setSearchPaths(searchPaths);


    auto audioEngine = SimpleAudioEngine::getInstance();
    audioEngine->preloadBackgroundMusic(fileUtils->fullPathForFilename("hura.mp3").c_str());
    audioEngine->preloadBackgroundMusic(fileUtils->fullPathForFilename("Mosquito.mp3").c_str());
    audioEngine->preloadBackgroundMusic(fileUtils->fullPathForFilename("babyCry.mp3").c_str());
    audioEngine->preloadEffect( fileUtils->fullPathForFilename("laser.mp3").c_str() );
    audioEngine->preloadEffect( fileUtils->fullPathForFilename("pop.mp3").c_str() );


    audioEngine->setBackgroundMusicVolume(0.5f);
    audioEngine->setEffectsVolume(0.5f);



    // create a scene. it's an autorelease object
    //auto scene = GameLayer::createScene();
    //auto scene = MemberStatesScene::createScene();
    //auto scene = GameLayer::createScene();
    //auto scene = CutScene1::createScene("en");
    auto scene = HelloWorld::createScene();
    // run
    director->runWithScene(scene);
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
