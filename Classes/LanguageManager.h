#ifndef LanguageManager_h
#define LanguageManager_h

#include <string>
using std::string;

#include "json/rapidjson.h"
#include "json/document.h"
#include "HelloWorldScene.h"
#include "GameLayer.h"
using namespace rapidjson;

class LanguageManager
{
private:
    Document _document;
    LanguageManager();
    ~LanguageManager();
    static LanguageManager* _instance;
    string fileName;
    string langName;
    string langType;




public:
    static LanguageManager* getInstance();
    string getStringForKey(string key) const;
    void setLanguage(string);



};

#endif
