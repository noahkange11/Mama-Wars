# ifndef __CUTSCENE1_H__
# define __CUTSCENE1_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "GameLayer.h"
#include "Definitions.h"



USING_NS_CC;
using namespace cocos2d;
using namespace CocosDenshion;

class CutScene1 : public Layer{

public:

  static Scene* createScene(string local);
  virtual bool init();
  void skipCutScene1(float dt);
  void GoToMenuLayer(float dt);
    string getLocal();

  CREATE_FUNC(CutScene1);



private:

  void initGoToItems();
  void menuBackCallBack(Ref* pSender);
  float _screenWidth;
  float _screenHeight;

  SpriteBatchNode* _gameBatchNode;
  Director* _director;
  Size _screenSize;


};
# endif // __CUTSCENE1_H__
